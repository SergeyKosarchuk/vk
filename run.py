from argparse import ArgumentParser
from monitor import app
from monitor import db
from monitor.updater import Updater



def pars_args():
    """Settings for argparse"""
    parser = ArgumentParser()
    parser.add_argument('-id', help='Input user_id to watch for.')
    return parser.parse_args()



if __name__ == '__main__':
    args = pars_args()
    if args.id:
        up = Updater(db, args.id)
        up.start()
    app.run(debug=True)
