
# Description #

Simple project written in Flask for self education. I used Vk api to get information 
about person plus his favorite music and movies from public page. I choosed SQLite 
to store user and content, Flask-SQLAlchemy ORM to handle DB operations and PyTest for unittests.

## Setup ##

$ cd ProjectFolder 
$ python3 -m venv venv 
$ . venv/bin/activate             # Bash terminal should now be with "(venv)" prefix.
$ pip install -r requirements.txt   # Install dependencies via pip.


## Usage ##

Just hit "python3 run.py" to start server.
Use key "-id" with desired user id to add new user and turn on auto update (10 min).

$ python run.py                     # Start without auto update.
$ python run.py -id <your id>       # Start with auto update.
$ pytest                            # Run tests.