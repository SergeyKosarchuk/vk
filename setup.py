from distutils.core import setup

setup(
    name='Vk-Monitor',
    version='1.0dev',
    author='Sergey Kosarchuk',
    author_email='sergeykosarchuk@gmail.com',
    packages=['monitor'],
    description='Simple project for self education',
    license='Public Domain',
    url='https://github.com/SergeyKosarchuk/Vk',
    long_description=open('README').read(),
)
