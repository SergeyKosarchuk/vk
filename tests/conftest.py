# import socket as s
from random import randint
from flask import Flask
from monitor.models import db as _db, User, Content
# from monitor.updater import Updater
import pytest


# @pytest.yield_fixture()
# def up():
#     _up = Updater(db, 1, time=2)
#     yield _up
#     _up.stop()


@pytest.fixture()
def user():
    _id = randint(1, 100)
    _name = 'test_name'
    _lastname = 'test_lastname'
    _status = 'normal'
    _user = User(id=_id, name=_name, lastname=_lastname, status=_status)     
    return _user


@pytest.fixture()
def content(user):
    _id = randint(1, 100)
    _user = user
    _content = Content(id=_id, name='test_content', type='m', user=_user) 
    return _content


@pytest.yield_fixture(scope='session')
def app():
    a = Flask('testing')
    a.testing = True
    with a.app_context():
        yield a


# @pytest.yield_fixture
# def socket():
#     _socket = s.socket(s.AF_INET, s.SOCK_STREAM)
#     yield _socket
#     _socket.close()


@pytest.fixture
def user_api():
    id_ = randint(1,20000)
    d = {'uid': id_,'first_name':'{}'.format(id_),'last_name':'{}'.format(id_),
        'movies': 'Test_Movie', 'music':'Test_Music'}
    return d 


@pytest.yield_fixture(scope='session')
def db(app):
    app.config.from_object('monitor.config.TestConfig')
    _db.app = app
    _db.init_app(app)
    _db.create_all()
    yield _db
    _db.drop_all()
    