from flask import render_template
from monitor import db
from .monitor import Monitor
from . import app



mn = Monitor(db)

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

# Show user profile for selected id.
@app.route('/user/<id>')
def show_user_profile(id):
    if mn.show(id) is None:
        return render_template('not_found.html')
    user, movies, music = mn.show(id)
    return render_template('index.html', user=user, movies=movies, music=music)