from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class User(db.Model):
    """ User class. """
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    lastname = db.Column(db.String(80))
    status = db.Column(db.String(10))
    content = db.relationship('Content', backref='user', 
                                            lazy='dynamic', 
                                            cascade="all, delete-orphan")

    # Constructor for API User object.
    @classmethod
    def from_api(cls, user_vk):
        id = user_vk['uid']
        name = user_vk['first_name']
        lastname = user_vk['last_name']
        status = cls.get_status(user_vk)
        return cls(id=id, name=name, lastname=lastname, status=status)

    @classmethod
    def get_status(cls, user_vk):
        if user_vk.get('deactivated'):
            return user['deactivated']
        elif user_vk.get('hidden'):
            return 'hidden'
        return 'normal'


    def __init__(self, id, name, lastname, status):
        self.id = id
        self.name = name
        self.lastname = lastname
        self.status = status

    def __repr__(self):
        return '{} {} {} {}'.format(int(self.id),
                                        self.name,
                                        self.lastname,
                                        self.status)


class Content(db.Model):
    __tablename__ = 'content'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    type = db.Column(db.String(1))
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))


    def __eq__(self, other):
        return self.name == other.name and self.type == other.type


    def __hash__(self):
        return hash(self.name + self.type)

    def __repr__(self):
        if self. user_id is not None:
            return 'id:{} name:{} type:{} user_id:{}'.format(int(self.id),
                                                                self.name,
                                                                self.type,
                                                                self.user_id
                                                            )

        return 'id:{} type:{} name:{}'.format(self.id, self.type, self.name)
