from flask import Flask
from .models import db


app = Flask(__name__)
app.config.from_object('monitor.config.ProductionConfig')
db.app = app
db.init_app(app)
db.create_all()


from . import views
