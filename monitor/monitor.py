import vk
from monitor.models import User, Content

class Monitor:
    """docstring fo Vk."""
    def __init__(self, db):
        self.session = vk.Session()
        self.api = vk.API(self.session)
        self.db = db

    # Delete old user and add new one.
    def update(self, id_):
        user = User.query.get(id_)
        if user is None:
            self.add_user(id_)
        else:
            user_vk = self.get_user_vk(id_)
            if user_vk:
                user.name = user_vk['first_name']
                user.lastname = user_vk['last_name']
                user.status = User.get_status(user_vk)
                old_content = set(user.content)
                new_content = set(self.get_content(user_vk))
                to_delete = list(old_content - new_content)
                to_add = list(new_content - old_content)
                for item in to_delete:
                    self.db.session.delete(item)
                self.db.session.add_all(to_add)
                self.db.session.commit()
            

    # Add new user to DB.
    def add_user(self, id_):
        user_vk = self.get_user_vk(id_)
        if user_vk:
            user = User.from_api(user_vk)
            user.content = self.get_content(user_vk)
            # Save user and his content to DB.
            self.db.session.add(user)
            self.db.session.commit()
        


    def get_music(self, id_):
        music = Content.query.filter_by(user_id = id_,type='m')
        return music


    def get_movies(self, id_):
        music = Content.query.filter_by(user_id = id_,type='f')
        return music


    def exists(self, id_):
        if User.query.get(id_):
            return True
        return False



    # Get information from DB.
    def show(self, id_):
        if self.exists(id_):
            user = User.query.get(id_)
            movies = self.get_movies(user.id)
            music = self.get_music(user.id)
            return user, movies, music
        return None


    # Get user info from Vk API.
    def get_user_vk(self, id_):
        try:
            user = self.api.users.get(user_ids=id_, fields='movies, music')[0]
        except vk.exceptions.VkAPIError:
            print('Invalid id')
            return None
        except:
            print('Connection Error')
            return None
        return user


    # Parse user content.
    def get_content(self, user_vk):
        content = []
        music = user_vk.get('music', None)
        movies = user_vk.get('movies', None)
        if music is not None and music != '':
            music = music.split(',')
            for m in music:
                c = Content(name=m, type='m')
                content.append(c)
        if movies is not None and movies != '':
            movies = movies.split(',')
            for m in movies:
                c = Content(name=m, type='f')
                content.append(c)
        return content
