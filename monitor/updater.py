from threading import Thread
from time import sleep
from .monitor import Monitor


class Updater:
    def __init__(self, db_, id_, time=600):
        self.id_ = id_
        self.time = time
        self._running = True
        self.mn = Monitor(db_)
        self.t = Thread(target=self.timer, daemon=True, name='Updater_{}'.format(id_))


    def update_user(self):
        if self.mn.exists(self.id_):
            self.mn.update(self.id_)
        else:
            self.mn.add_user(self.id_)


    def timer(self):
        while self._running is True:
            self.update_user()
            sleep(self.time)


    def start(self):
        self.t.start()


    def stop(self):
        self._running = False
